import React from 'react'

const Header = () => {
  return (
    <header className='header'>
      <div className='left-side'>
        <a href='/' style={{textDecoration:'none'}}> Where in the world</a>
      </div>
      <div className='right-side'>
        <button>Dark Mode</button>
      </div>
    </header>
  )
}

export default Header
