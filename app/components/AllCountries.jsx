'use client'

import Link from 'next/link'
import React from 'react'


const AllCountries = ({ countries }) => {
    console.log(countries)
  return (
    <div className='home_countries'>
      {countries.map(country => {
        const {
          name: { common: name },
          population,
          region,
          capital,
          ccn3,
          flags: { png: flags }
        } = country
        return (
          <div key={name} className='article'>
            <Link
              href={`/countries/${ccn3}`}
              style={{ textDecoration: 'none' }}
            >
              <img
                className='image'
                src={flags}
                alt={name}
                style={{ width: '100%', height: 200 }}
              />

              <div className='article-div'>
                <h3>{name}</h3>
                <p>
                  Population: <span>{population}</span>
                </p>
                <p>
                  Region: <span>{region}</span>{' '}
                </p>
                <p>
                  Capital: <span>{capital || 'unavailabale'}</span>{' '}
                </p>
              </div>
            </Link>
          </div>
        )
      })}
    </div>
  )
}

export default AllCountries
