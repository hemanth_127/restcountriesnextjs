import AllCountries from './components/AllCountries.jsx'
import Header from './components/Header.jsx'

async function getData () {
  const res = await fetch('https://restcountries.com/v3.1/all')

  if (!res.ok) {
    throw new Error('Failed to fetch data')
  }
  return res.json()
}

export default async function Home () {
  const countries = await getData()

  return (
    <div>
      <Header />
      <AllCountries countries={countries} />
    </div>
  )
}
